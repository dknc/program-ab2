# README #

This program ab is ported from https://code.google.com/archive/p/program-ab/
### 4.0.6-SNAPSHOT Changes ###
 * Add literal unicode pattern prefix "@JLUP::" for defining unicode pattern in normal.txt. Here are some unicode patterns for Chinese characters: comma, spaces etc.
 ```
  "@JLUP::\u2014"," "
  "@JLUP::\u2018"," "
  "@JLUP::\u2019"," "
  "@JLUP::\u201C"," "
  "@JLUP::\u201D"," "
  "@JLUP::\u2026"," "
  "@JLUP::\u3001"," "
  "@JLUP::\u3002"," "
  "@JLUP::\u3008"," "
  "@JLUP::\u3009"," "
  "@JLUP::\u300A"," "
  "@JLUP::\u300B"," "
  "@JLUP::\u3010"," "
  "@JLUP::\u3011"," "
  "@JLUP::\u3014"," "
  "@JLUP::\u3015"," "
  "@JLUP::\u300E"," "
  "@JLUP::\u300F"," "
  "@JLUP::\uFF08"," "
  "@JLUP::\uFF09"," "
  "@JLUP::\uFF0C"," "
  "@JLUP::\uFF1F"," "
  "@JLUP::\uFF1A"," "
  "@JLUP::\uFF1B"," "
 ```
### 4.0.5-beta Changes ###
 * Add reserved chat predicates MULTIPLE_SENTENCES_INPUT and ATE_MULTIPLE_SENTENCES_INPUT 
   * MULTIPLE_SENTENCES_INPUT    - The chat predicate key for the multiple sentences message
   * ATE_MULTIPLE_SENTENCES_INPUT   - The chat predicate key for setting a flag indicates if bot needs continue to handle the subsequent sentences.  Set it to true to let bot to ignore subsequent sentences in the same request and take the content matched in this category as the final response

### 4.0.5-beta Changes ###
 * Add reserved chat predicates MULTIPLE_SENTENCES_INPUT and ATE_MULTIPLE_SENTENCES_INPUT 
   * MULTIPLE_SENTENCES_INPUT    - The chat predicate key for the multiple sentences message
   * ATE_MULTIPLE_SENTENCES_INPUT   - The chat predicate key for setting a flag indicates if bot needs continue to handle the subsequent sentences.  Set it to true to let bot to ignore subsequent sentences in the same request and take the content matched in this category as the final response

### Changes ###
 * Replace System.Out.print with SL4J api
 * Make it able to create multiple Bots with independent configuration
 * Allow to register multiple AIMLProcessorExtension to AIMLProcessors

### License ###
[GNU Lessor License](http://www.gnu.org/licenses/lgpl.html)