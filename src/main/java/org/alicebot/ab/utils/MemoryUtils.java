package org.alicebot.ab.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MemoryUtils
{
  private static Logger log = LoggerFactory.getLogger(MemoryUtils.class);

  public static long totalMemory()
  {
    return Runtime.getRuntime().totalMemory();
  }

  public static long maxMemory()
  {
    return Runtime.getRuntime().maxMemory();
  }

  public static long freeMemory()
  {
    return Runtime.getRuntime().freeMemory();
  }
}
